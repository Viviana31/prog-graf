import { initializeApp } from 'firebase'

const app = initializeApp({
    apiKey: "AIzaSyDcFr3TM5aMsRJwwZI1axks8OzLB2q1uEE",
    authDomain: "semi14.firebaseapp.com",
    databaseURL: "https://semi14.firebaseio.com",
    projectId: "semi14",
    storageBucket: "",
    messagingSenderId: "760487002040",
    appId: "1:760487002040:web:30cf2c949a18c143"
});

export const db = app.database();
export const nameRef = db.ref('/nombres')