Vue.component('props-comp',{
    template: `
    <div class="container">
        <h1> Movie Lovers</h1>
        <div class="row">
            <div class="col-12 col-sm-6 col-md-4 col-lg-3"  v-for="(movie, key) in movies"
            :key="key">
                <MovieComp 
                :id="movie.id" 
                :nombre="movie.nombre" 
                :fecha="movie.fecha" 
                :descripcion="movie.descripcion" 
                :categoria="movie.descripcion" 
                :imagen="movie.imagen"
                :like.sync="movie.like"
               />
            </div>
        </div>
    </div>  
    `,
    data () {
        return {
            movies: [
                {
                    id: 1,
                    nombre:'Ode To Joy', 
                    fecha:'2019',
                    descripcion: 'Charlie intenta no ser feliz porque, debido a una enfermedad, cuando siente alegría pierde el control sobre su cuerpo. Sin embargo, cuando la mujer de sus sueños se enamora de él, Charlie no sabe cómo evitar estar alegre.',
                    categoria: 'Comedia Drama Romance',
                    imagen:'https://musicart.xboxlive.com/7/00155100-0000-0000-0000-000000000002/504/image.jpg?w=1920&h=1080',
                    like: false
                },
                {
                    id: 2,
                    nombre:'The Knight Before Christmas', 
                    fecha:'2019',
                    descripcion: 'Un caballero medieval británico es transportado mágicamente al presente, donde se enamora perdidamente de una profesora de ciencias que ha perdido la esperanza en el amor.',
                    categoria: 'Comedia Romance',
                    imagen:'https://pics.filmaffinity.com/The_Knight_Before_Christmas-972136948-large.jpg',
                    like: false
                },
                {
                    id: 3,
                    nombre:'Gemini Man', 
                    fecha:'2019',
                    descripcion: 'Un asesino a sueldo, demasiado mayor, decide retirarse. Pero esto no le va a resultar tan fácil, pues tendrá que enfrentarse a un clon suyo, mucho más joven.',
                    categoria: 'Acción Suspenso',
                    imagen:'https://as01.epimg.net/us/imagenes/2019/04/23/tikitakas/1556048083_533161_1556048763_sumario_normal.jpg',
                    like: false
                }
            ],
        }
    },
    components: {
        MovieComp,
    },
    methods: {
        onToggleLike (data) {
            let movieLike = this.movies.find(movie => movie.id == data.id)
            movieLike.like = data.like
        }
    },
}) 

