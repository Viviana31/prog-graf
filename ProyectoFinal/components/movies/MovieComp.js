let MovieComp = {
    template: `
    <div>
        <img :src="imagen" class="card-img-top">
        <h2 v-text="nombre"></h2>
        <h3 v-text="fecha"></h3>
        <p v-text="descripcion"></p>
        <button @click="$emit('update:like', !like)" v-text="like ? 'Favorita' : 'Agregar a Favoritas'"></button>
    </div>
    `,
    props: {
        id: {
            type: Number,
            required: true,
        },
        nombre: {
            type: String,
            required: true,
        },
        fecha:{
            type: String,
            required: true,
        },
        descripcion: {
            type: String,
        },
        imagen: {
            type: String,
            required: true,
        },
        like: {
            type:Boolean,
            required: true,
            default(){
                return false
            }
        }

    },
    methods: {
        toggleLike () {
        // this.like= !this.like
        let data = {
            id: this.id,
            like: !this.like
        }
        this.$emit('toggleLike', data)
        }
    },
}